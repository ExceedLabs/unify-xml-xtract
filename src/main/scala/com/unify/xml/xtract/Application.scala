package com.unify.xml.xtract

import com.unify.xml.xtract.service.{PostgresDBService, XmlHelper}

object Application extends App with XmlHelper {

  val path = "src/main/resources/Pricebook Secondary.xml"
  val response = extractPriceBooksFromXML(path)
  PostgresDBService.persistPriceBooks(response)

}
