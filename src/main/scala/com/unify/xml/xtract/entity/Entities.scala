package com.unify.xml.xtract.entity

import com.lucidchart.open.xtract.XmlReader._
import com.lucidchart.open.xtract.{XmlReader, __}
import play.api.libs.functional.syntax._

case class Amount(
  id: Option[Int],
  quantity: Int
)

case class PriceTable(
  productId: String,
  amountEntity: Amount,
  amount: Double
)

case class PriceTables(
  id: Option[Int],
  priceTables: Seq[PriceTable]
)

case class PriceBook(
  id: Option[Int],
  priceTables: PriceTables
)

case class PriceBooks(
  id: Option[Int],
  priceBooks: Seq[PriceBook]
)



object Amount {
  implicit val reader: XmlReader[Amount] = (
    attribute[Int]("id").optional and
      attribute[Int]("quantity")
  ) (apply _)
}

object PriceTable {
  implicit val reader: XmlReader[PriceTable] = (
    attribute[String]("product-id") and
      (__ \ "amount").lazyRead(first[Amount]) and
      (__ \ "amount").read[Double]
  ) (apply _)
}

object PriceTables {
  implicit val reader: XmlReader[PriceTables] = (
    attribute[Int]("id").optional and
      (__ \ "price-table").lazyRead(seq[PriceTable]).default(Nil)
  ) (apply _)
}

object PriceBook {
  implicit val reader: XmlReader[PriceBook] = (
    attribute[Int]("id").optional and
      (__ \ "price-tables").lazyRead(first[PriceTables])
    ) (apply _)
}

object PriceBooks {
  implicit val reader: XmlReader[PriceBooks] = (
    attribute[Int]("id").optional and
      (__ \ "pricebook").lazyRead(seq[PriceBook]).default(Nil)
  ) (apply _)
}
