package com.unify.xml.xtract.connection

import java.sql.{Connection, DriverManager}
import java.util.Properties

import com.unify.xml.xtract.util.Environment

import scala.util.Try

object ConnectionFactory {

  def getPostgresConnection(properties: Properties): Try[Connection] = {
    val driver = properties.getProperty(Environment.postgresDriver)
    val host = properties.getProperty(Environment.postgresHost)
    val port = properties.getProperty(Environment.postgresPort)
    val database = properties.getProperty(Environment.postgresDatabase)
    val jdbcUrl = s"jdbc:postgresql://$host:$port/$database"
    val username = properties.getProperty(Environment.postgresUsername)
    val password = properties.getProperty(Environment.postgresPassword)
    val connectionProps = new Properties()

    connectionProps.setProperty("user", username)
    connectionProps.setProperty("password", password)
    connectionProps.setProperty("ssl", "true")
    connectionProps.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory")

    Class.forName(driver)

    Try(DriverManager.getConnection(jdbcUrl, connectionProps))
  }

}
