package com.unify.xml.xtract.util

import java.io.FileInputStream
import java.util.Properties

object Environment {

  val postgresDriver = "postgres.driver"
  val postgresHost = "postgres.credentials.host"
  val postgresPort = "postgres.credentials.port"
  val postgresDatabase = "postgres.credentials.database"
  val postgresUsername = "postgres.credentials.username"
  val postgresPassword = "postgres.credentials.password"

  def getProperties: Properties = {
    val properties: Properties = new Properties()
    val fin = new FileInputStream("../config/unify-xml-xtract-current.properties")

    properties.load(fin)

    properties
  }

}
