package com.unify.xml.xtract.service

import java.sql.ResultSet

import com.unify.xml.xtract.connection.ConnectionFactory
import com.unify.xml.xtract.entity.PriceBooks
import com.unify.xml.xtract.util.Environment
import org.slf4j.{Logger, LoggerFactory}

object PostgresDBService {

  val properties = Environment.getProperties

  def persistPriceBooks(pricebooks: Option[PriceBooks]): Unit = {
    val connectionPostgres = ConnectionFactory.getPostgresConnection(properties)

    if(pricebooks.isDefined) {
      logger.info("***Price books: " + pricebooks.get)

      val query = "INSERT INTO FROM salesforce.pricebookentry (one, two, three) VALUES (?, ?, ?)"
      val statement = connectionPostgres.get.prepareStatement(query)

      pricebooks.get.priceBooks.head.priceTables.priceTables.map(priceTable => {
        statement.setString(1, priceTable.productId)
        statement.setString(2, priceTable.productId)
        statement.setString(3, priceTable.productId)

        /*val resultPostgres = statement.executeQuery()

        if(resultPostgres.isInstanceOf[ResultSet]) {
          logger.info(s"SUCCESS: ${statement.toString}")
        } else {
          logger.error(s"FAIL: ${statement.toString}")
        }*/
      })

      connectionPostgres.get.close()
    }
  }

  def logger: Logger = LoggerFactory.getLogger(this.getClass)

}
