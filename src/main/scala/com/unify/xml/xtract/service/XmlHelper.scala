package com.unify.xml.xtract.service

import java.io.File

import com.lucidchart.open.xtract.XmlReader
import com.unify.xml.xtract.entity.PriceBooks
import org.slf4j.{Logger, LoggerFactory}

import scala.io.Source
import scala.xml.XML

/**
  * This class provide functionality to parse xml data into scala case classes
  */
trait XmlHelper {

  def extractPriceBooksFromXML(filePath: String): Option[PriceBooks] = {
    val xmlData = Source.fromFile(new File(filePath)).getLines().mkString("\n")

    logger.info("***File to be parsed: ")
    logger.info(xmlData)

    val xml = XML.loadString(xmlData)
    XmlReader.of[PriceBooks].read(xml).toOption
  }

  def logger: Logger = LoggerFactory.getLogger(this.getClass)

}
