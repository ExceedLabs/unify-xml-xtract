name := "unify-xml-xtract"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.lucidchart" %% "xtract" % "1.1.1",
  "com.typesafe.play" % "play-json_2.11" % "2.6.7",
  "org.scalatest" %% "scalatest" % "3.0.4",
  "org.slf4j" % "slf4j-simple" % "1.7.25",
  "postgresql" % "postgresql" % "9.4.1208-jdbc42-atlassian-hosted"
)